import GameServerProfile from "./profile";
import { shell, ipcRenderer } from "electron";
import { switchPage } from "../customEvents";
import { NewsType } from "./news";
import { makeOnlineCounterList, OnlineListParser } from "./onlineCount";
import { socialLink } from "./socialLink";
import { PageController } from "./serverView/controller";

export default class GameServer {
  public pageController: PageController;

  constructor(
    public profile: GameServerProfile,
    public menuName: string,
    public name: string,
    public shortDescription: string, // the server in 1-2 sentences
    public newsPageUrl: string,
    public newsLatestPage: { url: string; type: NewsType },
    public backgrounds: { isVideo: boolean; file: string }[],
    public icon: string,
    public socialLinks: socialLink[],
    public TOSLink: string,
    public OnlineList: { parser: OnlineListParser; url: string }
  ) {
    this.pageController = new PageController(this);
  }

  getMenuEntry(): HTMLElement {
    const sidebarItem = document.createElement("div");
    sidebarItem.classList.add("sidebarItem");
    //Online counter
    const OnlineCounterContainer = document.createElement("div");

    //OnlineCounterContainer.appendChild(makeOnlineCounterList(this));
    sidebarItem.appendChild(OnlineCounterContainer);
    //Title
    const title = document.createElement("div");
    title.classList.add("title");

    const titleIMG = document.createElement("img");
    titleIMG.src = `./media/server/${this.icon}`;

    const titleText = document.createElement("div");
    titleText.innerText = this.menuName;

    title.appendChild(titleIMG);
    title.appendChild(titleText);

    title.addEventListener("click", () => {
      switchPage("SERVER", this.name, "INFO");
    });
    if (this.profile.address != "noServer") {
      let onlineBoxActive: boolean = false;
      let onlineBoxSchouldBeActive: boolean = false;
      let updateOnlineContainer = async () => {
        if (onlineBoxSchouldBeActive == onlineBoxActive) return;
        onlineBoxActive = onlineBoxSchouldBeActive;
        while (OnlineCounterContainer.firstChild) {
          OnlineCounterContainer.removeChild(OnlineCounterContainer.firstChild);
        }
        if (onlineBoxSchouldBeActive) {
          OnlineCounterContainer.appendChild(await makeOnlineCounterList(this));
        }
        console.log(onlineBoxSchouldBeActive);
      };

      let SetUpdate = (state: boolean) => {
        onlineBoxSchouldBeActive = state;
        setTimeout(() => {
          updateOnlineContainer();
        }, 160);
      };

      title.addEventListener("mouseover", () => {
        SetUpdate(true);
      });

      title.addEventListener("mouseout", () => {
        SetUpdate(false);
      });

      OnlineCounterContainer.addEventListener("mouseover", () => {
        SetUpdate(true);
      });

      OnlineCounterContainer.addEventListener("mouseout", () => {
        SetUpdate(false);
      });
    }

    sidebarItem.appendChild(title);

    //Collapsable
    const container = document.createElement("div");
    container.classList.add("colapse");

    const info = document.createElement("div");
    info.innerHTML = '<i class="fas fa-info fa-fw"></i>Information';
    container.appendChild(info);

    info.addEventListener("click", () => {
      switchPage("SERVER", this.name, "INFO");
    });

    const news = document.createElement("div");
    news.innerHTML =
      '<i class="far fa-newspaper fa-fw"></i>News <i class="fas fa-external-link-alt"  data-fa-transform="shrink-2"></i>';
    if (this.newsPageUrl && this.newsPageUrl !== null) {
      news.addEventListener("click", () => {
        shell.openExternal(this.newsPageUrl);
      });
    } else {
      news.hidden = true;
    }

    container.appendChild(news);

    const screenshots = document.createElement("div");
    screenshots.innerHTML = '<i class="fas fa-camera fa-fw"></i>Screenshots';
    container.appendChild(screenshots);

    screenshots.addEventListener("click", () => {
      switchPage("SERVER", this.name, "SCREENSHOTS");
    });
    const preferences = document.createElement("div");
    if (this.profile.address != "noServer") {
      preferences.innerHTML =
        '<i class="fas fa-sliders-h fa-fw"></i>Preferences';
      preferences.addEventListener("click", () => {
        switchPage("SERVER", this.name, "PREF");
      });
      container.appendChild(preferences);
    }
    sidebarItem.appendChild(container);
    //event Target / interactivity
    const events = document.createElement("span");
    events.classList.add("switch-page-event");
    events.addEventListener("site-changed", (event: CustomEvent) => {
      sidebarItem.classList.remove("selected");
      info.classList.remove("selected");
      //news.classList.remove("selected");
      screenshots.classList.remove("selected");
      preferences.classList.remove("selected");
      if (
        event.detail.sitetype === "SERVER" &&
        event.detail.page === this.name
      ) {
        sidebarItem.classList.add("selected");
        // if(event.detail.subPage === "NEWS") {
        //   news.classList.add("selected");
        // } else
        if (event.detail.subPage === "PREF") {
          preferences.classList.add("selected");
        } else if (event.detail.subPage === "SCREENSHOTS") {
          screenshots.classList.add("selected");
        } else {
          info.classList.add("selected");
        }
      }
    });

    sidebarItem.appendChild(events);
    return sidebarItem;
  }

  play() {
    const args: any = JSON.parse(JSON.stringify(this.profile));
    args.username = localStorage.getItem("2_username_" + this.profile.address);
    args.password = localStorage.getItem("2_pin_" + this.profile.address);
    ipcRenderer.send("play", args);
  }
}
