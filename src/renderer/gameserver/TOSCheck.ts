import GameServer from "./server";
import { ipcRenderer } from "electron";

let LAST_TOS = "";

export default async function CheckNAcceptTOS(
  server: GameServer
): Promise<boolean> {
  try {
    let thisVersion = await getTOSVersion(server.TOSLink);
    console.log(server.TOSLink, thisVersion);
    LAST_TOS = thisVersion;
    return localStorage.getItem(server.TOSLink) == thisVersion;
  } catch (err) {
    console.log("TOS check Error", err);
    return true;
  }
}

async function getTOSVersion(address: string): Promise<string> {
  // Returns date or if not found the hash of the whole page
  const PageContent = await request(address);
  const date = PageContent.match(/Last Update: \d\d\d\d-\d\d-\d\d/g);
  let hash: string;
  if (!date || date.length > 1) {
    hash = HashString(
      PageContent.replace(/ |\t|\n/g, "").replace(/<.+?>/g, "r")
    ).toString(16);
  } else {
    hash = date[0].replace(/ /g, "");
  }

  return hash;
}

function request(url: string): Promise<string> {
  return new Promise((res, rej) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.addEventListener("error", (ev) => {
      rej(ev);
    });
    xhr.onload = function () {
      if (xhr.status === 200) {
        res(xhr.responseText);
      } else {
        rej(new Error(`xhr.status: ${xhr.status} != 200`));
      }
    };
    xhr.send();
  });
}

// Hash function from http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
function HashString(s: string) {
  let hash = 0;
  if (s.length == 0) return hash;
  for (let i = 0; i < s.length; i++) {
    let char = s.charCodeAt(i);
    hash = (hash << 5) - hash + char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

export function acceptLastTOS(server: GameServer) {
  localStorage.setItem(server.TOSLink, LAST_TOS);
}
