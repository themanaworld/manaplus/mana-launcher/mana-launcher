import GameServer from "./server";

const MAX_SHOWN_NAMES = 5;

export async function makeOnlineCounterList(
  server: GameServer
): Promise<HTMLElement> {
  try {
    const playerList = await fetchOnlineList(server);
    return generateHTML(playerList);
  } catch (err) {
    console.log(err);
    const OnlineCounterContainer = document.createElement("span");
    OnlineCounterContainer.classList.add("onlineCounter");
    OnlineCounterContainer.innerHTML = `<i class="fas fa-times fa-xs"></i>Error:<br>${err}`;
    return OnlineCounterContainer;
  }
}

function generateHTML(onlinePlayers: string[]): HTMLElement {
  const OnlineCounterContainer = document.createElement("span");
  OnlineCounterContainer.classList.add("onlineCounter");
  OnlineCounterContainer.innerHTML = `<i class="fas fa-users fa-xs"></i>${onlinePlayers.length} Players online<br>`;

  const PlayerList = document.createElement("span");
  PlayerList.classList.add("playerList");

  let displayed_players =
    MAX_SHOWN_NAMES < onlinePlayers.length
      ? onlinePlayers.slice(0, MAX_SHOWN_NAMES)
      : onlinePlayers;

  displayed_players.forEach((playername) => {
    const PlayerItem = document.createElement("span");
    PlayerItem.classList.add("playerItem");
    PlayerItem.innerHTML = `<i class="fas fa-circle fa-xs" data-fa-transform="shrink-8 left-2"></i>${playername}`;
    PlayerList.appendChild(PlayerItem);
  });

  if (MAX_SHOWN_NAMES < onlinePlayers.length) {
    const hiddenPlayers = onlinePlayers.filter(
      (playername) => displayed_players.indexOf(playername) == -1
    );
    const MorePlayers = document.createElement("span");
    MorePlayers.classList.add("morePlayers");
    MorePlayers.innerText = ` and ${
      onlinePlayers.length - MAX_SHOWN_NAMES
    } more..`;
    MorePlayers.title = hiddenPlayers.join("\n");

    PlayerList.appendChild(MorePlayers);
  }

  OnlineCounterContainer.appendChild(PlayerList);
  return OnlineCounterContainer;
}

export enum OnlineListParser {
  Invalid,
  TMW,
  TMW2API,
  EXAMPLE_DATA,
  JSON_ARRAY,
}

async function fetchOnlineList(server: GameServer): Promise<string[]> {
  if (!server.OnlineList)
    throw new Error("No Online list was\n specified for this Server");

  if (server.OnlineList.parser && server.OnlineList.url) {
    switch (server.OnlineList.parser) {
      case OnlineListParser.EXAMPLE_DATA:
        return ["LawnCable", "Saulc GM", "Crazyfefe", "Jesus Saves", "DUSTMAN"];
      case OnlineListParser.TMW:
        return tmwParser(await request(server.OnlineList.url));
      case OnlineListParser.JSON_ARRAY:
        return jsonArrayParser(await request(server.OnlineList.url));
      default:
        throw new Error("Online List Parser is unknown");
    }
  } else {
    throw new Error("Online List wasn't configured right");
  }
}

function tmwParser(rawData: string): string[] {
  let stringArray: string[] = [];
  rawData.replace(/<td>(.+?)<\/td>/g, (x: string, content: string) => {
    stringArray.push(content.replace(/<\/{0,1}.+?>/g, ""));
    return "";
  });
  return stringArray;
}

function jsonArrayParser(rawData: string): string[] {
  return JSON.parse(rawData);
}

function request(url: string): Promise<string> {
  return new Promise((res, rej) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.addEventListener("error", (ev) => {
      console.log("Probably a network error:", ev);
      rej(new Error("Probably a network error"));
    });
    xhr.onload = function () {
      if (xhr.status === 200) {
        res(xhr.responseText);
      } else {
        console.log(`xhr.status: ${xhr.status} != 200`);
        rej(new Error("Probably a network error"));
      }
    };
    xhr.send();
  });
}
