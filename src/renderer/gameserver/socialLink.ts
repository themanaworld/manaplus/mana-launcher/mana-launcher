import { shell } from "electron";

export class socialLink {
  constructor(
    public icon: string, // has to be one from font awesome -https://fontawesome.com/icons
    public tooltip: string,
    public url: string
  ) {}

  getHTML(): HTMLElement {
    //<button onclick="sv.openSocialLink(this)" socialLink="abc"><i class="fa fa-user"></i></button><br>
    const element = document.createElement("button");
    element.onclick = () => {
      this.open();
    };
    element.innerHTML = `<i class="${this.icon}"></i>`;
    element.title = this.tooltip;
    return element;
  }

  open() {
    console.log("A link was clicked!", this.url);
    if (this.url)
      shell.openExternal(
        this.url.indexOf("://") !== -1 ? this.url : `https://${this.url}`
      );
  }
}
