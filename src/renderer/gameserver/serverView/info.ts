import { News } from "../news";
import { shell } from "electron";
import { GameServerPage } from "./serverPage";

export class ServerInfoPage extends GameServerPage {
  public getPage(): HTMLElement {
    const content = document.createElement("div");
    content.classList.add("infoServerPage");

    const title = document.createElement("div");
    title.classList.add("title");
    title.innerText = this.server.name;
    content.appendChild(title);

    const socialContainer = document.createElement("div");
    socialContainer.classList.add("socialContainer");
    setTimeout(() => {
      this.server.socialLinks.forEach((link) => {
        socialContainer.appendChild(link.getHTML());
      });
    }, 5);
    content.appendChild(socialContainer);

    const shrtDsrption = document.createElement("div");
    shrtDsrption.classList.add("shortDescription");
    shrtDsrption.innerText = this.server.shortDescription;
    content.appendChild(shrtDsrption);
    if (this.server.newsLatestPage && this.server.newsLatestPage != null) {
      const latestNews = document.createElement("div");
      latestNews.classList.add("news");
      content.appendChild(latestNews);
      News.get(
        this.server.newsLatestPage.url,
        this.server.newsLatestPage.type
      ).then((result: string) => {
        latestNews.innerHTML = result;
        const aTags = latestNews.getElementsByTagName("a");
        for (var i = 0; i < aTags.length; i++) {
          const href = aTags[i].href.toString();
          aTags[i].addEventListener("click", () => {
            shell.openExternal(href);
          });
          aTags[i].href = "#";
        }
      });
    }

    return content;
  }
}
