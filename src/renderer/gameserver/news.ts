export enum NewsType {
  Markdown, // Without html + yaml
  ManaPlus, //update news txt
}

export namespace News {
  // Fetches only the most recent entry
  export async function get(url: string, parser: NewsType): Promise<string> {
    try {
      //1. load
      const unsafe_content = await request(`${url}?${Math.random()}`);
      //2. sanitize
      const content = killHTML(unsafe_content);
      //3. parse
      if (parser == NewsType.ManaPlus) {
        return manaTextParser(content);
      } else if (parser == NewsType.Markdown) {
        return "Parsing Failed: Markdown Parser is not implemented yet";
      }
    } catch (e) {
      console.log(e);
      return `Failed to get the news, please select the news category on the right to view all news`;
    }
  }
}

function request(url: string): Promise<string> {
  return new Promise((res, rej) => {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("error", (ev) => {
      rej(ev);
    });
    xhr.open("GET", url);
    xhr.onload = function () {
      if (xhr.status === 200) {
        res(xhr.responseText);
      } else {
        rej(new Error(`xhr.status: ${xhr.status} != 200`));
      }
    };
    xhr.send();
  });
}

function killHTML(raw: string): string {
  return raw.replace(/<|>/, "⚠️");
}

function manaTextParser(input: string) {
  const tmwLegacy = "##7Legacy Server##0";
  let result = input;
  if (result.indexOf(tmwLegacy) !== -1) {
    const i = result.indexOf(tmwLegacy) + tmwLegacy.length;
    result = result.slice(0, result.indexOf(tmwLegacy, i));
    result = result.replace(/\n \n /, "");
  }
  result = result
    .replace(/\[@@(.+?)\|(.+?)@@\]/g, '<a href="$1">$2</a>')
    .replace(/##B(.+?)##b/g, "<b>$1</b>")
    .replace(/##0 Actual Release: ##1 *(.+)/, "<h2>$1</h2>")
    .replace(/\n/g, "<br>");
  if (result.indexOf(tmwLegacy) === -1)
    result = result
      .replace(/br>([^]+?)<br></g, "br><p>$1</p><br><")
      .replace(/<br>/g, "");
  return result.replace(/##\d/g, "");
}
