import { ServerSubPages } from "./gameserver/serverView/controller";

export function switchPage(
  sitetype: "SERVER" | "INTERN",
  page: string,
  subPage?: ServerSubPages
) {
  const event = new CustomEvent("site-changed", {
    detail: { sitetype, page, subPage },
  });
  const elements = document.getElementsByClassName("switch-page-event");
  for (let i = 0; i < elements.length; i++) {
    elements[i].dispatchEvent(event);
  }
}
