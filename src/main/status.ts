import { mainWindow } from "../main";
import { dialog } from "electron";

type STATUS = {
  progress: number;
  activity: string;
  ActivityIsError: boolean;
  playing: boolean;
  gameRunning: boolean;
  gameStatus: {
    server: string;
  };
};

const status: STATUS = {
  progress: null,
  activity: null,
  playing: false, //Is manaplus starting or started
  ActivityIsError: false,
  gameRunning: false,
  gameStatus: {
    server: "Launcher",
  },
};

export namespace Status {
  export function setGameRunning(value: boolean) {
    status.gameRunning = value;
    updateStatus();
  }
  export function setProgress(value: number) {
    status.progress = value;
    updateStatus();
  }
  export function removeProgress() {
    status.progress = null;
    updateStatus();
  }
  export function setActivity(value: string) {
    status.activity = value;
    status.ActivityIsError = false;
    updateStatus();
  }
  export function removeActivity() {
    status.activity = null;
    status.ActivityIsError = false;
    updateStatus();
  }
  export function showError(
    title: string,
    message: string,
    activityMsg: string = message
  ) {
    status.activity = activityMsg;
    status.ActivityIsError = true;
    updateStatus();
    setTimeout(() => {
      dialog.showErrorBox(title, message);
    }, 300);
  }
  export function setPlaying(playing: boolean) {
    status.playing = playing;
    updateStatus();
  }
  export function setGameStatus(gameStatus: { server: string }) {
    status.gameStatus = gameStatus;
    updateStatus();
  }
  export function getStatus(): STATUS {
    return status;
  }
}

function updateStatus() {
  if (mainWindow && mainWindow !== null) {
    if (status.progress == null || status.progress < 0)
      mainWindow.setProgressBar(-1);
    else if (status.progress > 100) mainWindow.setProgressBar(2);
    else mainWindow.setProgressBar(status.progress / 100);

    mainWindow.webContents.send("status-update", status);
  }
  EventEmitter.emit("status", status);
}

import * as events from "events";

class MyEmitter extends events {}

export const EventEmitter = new MyEmitter();
