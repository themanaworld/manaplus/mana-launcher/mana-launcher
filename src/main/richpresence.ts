import { Status } from "./status";
// Only for testing as of right now -> Experimental
const ClientId = "551884486351126528";
const DiscordRPC = require("discord-rich-presence")(ClientId);

const slogans = [
  "free OpenSource 2D MMORPG",
  "Community made",
  "Join a Server or start your own",
];
const dataSet: {
  [key: string]: { name: string; logo: string; description: string };
} = {
  "server.tmw2.org": {
    name: "Moubootaur Legends",
    logo: "tmw2",
    description: "Playing on Moubootaur Legends ⎛tmw2.org⎠",
  },
  "world.evolonline.org": {
    name: "Evol Online",
    logo: "evol",
    description: "Playing on Evol Online ⎛evolonline.org⎠",
  },
  "server.themanaworld.org": {
    name: "The Mana World",
    logo: "tmw",
    description: "Playing on The Mana World ⎛themanaworld.org⎠",
  },
  noServer: {
    name: "Manaplus",
    logo: "manaplus",
    description: "Playing on any of the M+ servers",
  },
};

async function setActivity() {
  const status = Status.getStatus();

  const slogan = slogans[Math.floor(Math.random() * slogans.length)];

  const data = dataSet[status.gameStatus.server];

  const details =
    status.gameStatus.server === "Launcher"
      ? "in Launcher Menu"
      : `🎮 ${data.name} 🎮`;
  const logo = data && data.logo;

  DiscordRPC.updatePresence({
    state: `»${slogan}«`,
    details,
    largeImageKey: logo,
    largeImageText: data && data.description,
    //smallImageKey: ,
    //smallImageText: 'string',
    //partyId: 'ae488379-351d-4a4f-ad32-2b9b01c91657',
    //partySize: 1,
    //partyMax: 999,
    //matchSecret: 'string',
    //joinSecret: 'string',
    //spectateSecret: 'string',
    //startTimestamp
  });
}

DiscordRPC.on("connected", () => {
  setActivity();

  // activity can only be set every 15 seconds
  setInterval(() => {
    setActivity();
  }, 15e3);
});

DiscordRPC.on("error", console.log);

DiscordRPC.on("join", (secret: string) => {
  console.log("we should join with", secret);
});

DiscordRPC.on("spectate", (secret: string) => {
  console.log("we should spectate with", secret);
});

// DiscordRPC.on('joinRequest', (user) => {
//   if (user.discriminator === '1337') {
//     DiscordRPC.reply(user, 'YES');
//   } else {
//     DiscordRPC.reply(user, 'IGNORE');
//   }
// });

export function quit() {
  DiscordRPC.disconnect();
  console.log("Shutting down Discord RPC integration");
}

process.on("unhandledRejection", console.error);
