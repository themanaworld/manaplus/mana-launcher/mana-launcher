import { ManaPlusApp } from "./manaApp.interface";
import { app } from "electron";
import * as fs from "fs-extra";
import { getRequest } from "../../util/webrequest";
import { Status } from "../../status";
import { download, Progress as ProgressType } from "../../util/downloader";
import { promisify } from "util";

export class ManaPlusAppLinux implements ManaPlusApp {
  private path: string;
  startCommand: string;
  versionRegEx: RegExp = /.*ManaPlus ([\d.]+) Linux.*/g; //TODO
  constructor() {
    const ManaPath = app.getPath("userData") + "/manaplus";
    fs.existsSync(ManaPath) || fs.mkdirSync(ManaPath);
    fs.existsSync(app.getPath("userData") + "/temp") ||
      fs.mkdirSync(app.getPath("userData") + "/temp");
    this.path = ManaPath;
    this.startCommand = ManaPath + "/Mana.AppImage";
  }
  getGameDir(): string {
    throw new Error("getGameDir() is windows only!");
  }
  getVersion(): Promise<string> {
    return new Promise((res, rej) => {
      let output: string;
      const child = require("child_process").execFile(
        this.startCommand,
        ["-v"],
        function (err: Error, data: any) {
          output = data.toString();
        }
      );
      child.on("close", () => {
        output = output.replace(this.versionRegEx, "$1");
        res(output);
      });
      child.on("error", () => {
        rej(new Error("Version check failed"));
      });
    });
  }
  isInstalled(): boolean {
    return fs.existsSync(this.path + "/Mana.AppImage");
  }
  async updateAvailable(): Promise<{
    isNewVersion: boolean;
    newestVersion: string;
  }> {
    try {
      let versions = await getRequest(
        "https://tmw2.org/manalauncher/versions.json?" + Date.now()
      );
      let currect_version = (await this.isInstalled)
        ? await this.getVersion()
        : "-";
      return {
        isNewVersion: currect_version.indexOf(versions.AppImage.version) === -1,
        newestVersion: versions.AppImage.version,
      };
    } catch (e) {
      throw e;
    }
  }
  async update(): Promise<any> {
    fs.existsSync(app.getPath("userData") + "/temp") ||
      fs.mkdirSync(app.getPath("userData") + "/temp");
    // Get Update URL
    Status.setProgress(500);
    Status.setProgress(-1);
    Status.setActivity("Fetching Download URL");
    let downloadURL;
    try {
      let versions = await getRequest(
        "https://tmw2.org/manalauncher/versions.json?" + Date.now()
      );
      downloadURL = versions.AppImage.file;
    } catch (e) {
      console.log(e);
      Status.showError(
        "Download Url fetching error",
        e.message,
        `Download Url fetching error: ${e.message}`
      );
      throw new Error("Download Url fetching error");
    }
    Status.setProgress(-1);

    const updateDestination: string = `${app.getPath(
      "userData"
    )}/temp/update.AppImage`;

    try {
      await download(downloadURL, updateDestination, (state: ProgressType) => {
        Status.setProgress(Math.floor(state.percent * 100));
        const speed = Math.floor(Math.floor(state.speed) / 1024);
        Status.setActivity(`Downloading ManaPlus... ${speed} KiB/s`);
        console.log(state);
      });
    } catch (e) {
      console.log(e);
      Status.showError(
        "Download error",
        e.message,
        `Download error: ${e.message}`
      );
      throw new Error("Download error");
    }
    Status.setProgress(500);

    //IDEA: Check Integrity of the download

    // Backup old files
    Status.setActivity(`Backup Old version`);
    try {
      await fs.remove(this.path + "/Mana2.AppImage");
      if (fs.existsSync(this.path + "/Mana.AppImage"))
        await fs.move(
          this.path + "/Mana.AppImage",
          this.path + "/Mana2.AppImage"
        );
      console.log("Backup old version done.");
    } catch (err) {
      Status.showError(
        "Backup old version Failed",
        err.message,
        `Backup old version Failed: ${err.message}`
      );
      throw new Error("Backup error");
    }

    Status.setProgress(500);
    Status.setActivity(`ManaPlus download completed. Instaling..`);
    try {
      console.log("Use chmod");
      const chmod = promisify(fs.chmod);
      await chmod(updateDestination, "755");

      console.log("Now move the thing!");
      await fs.move(updateDestination, this.path + "/Mana.AppImage");
      //await chmod(this.path + '/Mana.AppImage', '744');
    } catch (err) {
      console.log("Instalation error", err);
      Status.showError(
        "Instalation failed",
        err ? err.message : "undefined",
        `Instalation Failed: ${err ? err.message : "undefined"}`
      );
      throw new Error("Instalation error");
    }

    Status.setActivity("Instalation completed");

    //IDEA: Check Integrity of gamefiles

    Status.setActivity("Update successfull");

    return 0;
  }
}
