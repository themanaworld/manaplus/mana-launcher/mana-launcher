import { ManaPlusApp } from "./manaApp.interface";
import { app } from "electron";
import * as fs from "fs-extra";
import { Status } from "../../status";
import { download, Progress as ProgressType } from "../../util/downloader";
import * as extract from "extract-zip";
import { getRequest } from "../../util/webrequest";

export class ManaPlusAppWindows implements ManaPlusApp {
  private path: string;
  public readonly startCommand: string;
  versionRegEx: RegExp = /.*ManaPlus ([\d.]+) Windows.*/g;
  constructor() {
    const ManaPath = app.getPath("userData") + "\\manaplus";
    fs.existsSync(ManaPath) || fs.mkdirSync(ManaPath);
    fs.existsSync(app.getPath("userData") + "\\temp") ||
      fs.mkdirSync(app.getPath("userData") + "\\temp");
    this.path = ManaPath;
    this.startCommand = this.path + "\\Mana\\manaplus.exe";
  }

  getGameDir(): string {
    return this.path + "\\Mana\\";
  }
  getVersion(): Promise<string> {
    return new Promise((res, rej) => {
      let output: string;
      const child = require("child_process").execFile(
        this.startCommand,
        ["-v"],
        function (err: Error, data: any) {
          output = data.toString();
        }
      );
      child.on("close", () => {
        output = output.replace(this.versionRegEx, "$1");
        res(output);
      });
      child.on("error", () => {
        rej(new Error("Version check failed"));
      });
    });
  }
  isInstalled(): boolean {
    return fs.existsSync(this.path + "\\Mana\\manaplus.exe");
  }

  async update() {
    fs.existsSync(app.getPath("userData") + "\\temp") ||
      fs.mkdirSync(app.getPath("userData") + "\\temp");
    // Get Update URL
    Status.setProgress(500);
    Status.setActivity("Fetching Download URL");
    let downloadURL;
    try {
      let versions = await getRequest(
        "https://tmw2.org/manalauncher/versions.json?" + Date.now()
      );
      downloadURL = versions.windows64.file;
    } catch (e) {
      console.log(e);
      throw new Error("Download Url fetching error");
    }
    Status.setProgress(-1);

    const updateDestination: string = `${app.getPath(
      "userData"
    )}\\temp\\update.zip`;
    try {
      await download(downloadURL, updateDestination, (state: ProgressType) => {
        Status.setProgress(Math.floor(state.percent * 100));
        const speed = Math.floor(Math.floor(state.speed) / 1024);
        Status.setActivity(`Downloading ManaPlus... ${speed} KiB/s`);
        console.log(state);
      });
    } catch (e) {
      console.log(e);
      throw new Error("Download error");
    }
    Status.setProgress(500);
    //IDEA: Check Integrity of the download

    // Backup old files
    Status.setActivity(`Backup Old version`);
    try {
      await fs.remove(this.path + "\\Mana2");
      if (fs.existsSync(this.path + "\\Mana"))
        await fs.move(this.path + "\\Mana", this.path + "\\Mana2");
      console.log("Backup old version done.");
    } catch (err) {
      Status.showError(
        "Backup old version Failed",
        err.message,
        `Backup old version Failed: ${err.message}`
      );
      throw new Error("Backup error");
    }

    Status.setProgress(500);
    Status.setActivity(`ManaPlus download completed. Unziping..`);
    const extraction = new Promise((resolve, reject) => {
      extract(updateDestination, { dir: this.path }, function (err) {
        if (err) {
          console.log(err);
          Status.showError(
            "ManaPlus unziping failed",
            err.message,
            `Unzip Failed: ${err.message}`
          );
          reject(new Error("Extraction Error"));
        }
        resolve();
      });
    });
    await extraction;

    Status.setActivity("Unziping completed");

    //IDEA: Check Integrity of gamefiles

    // DELETE Old File and remove update file
    Status.setActivity("Cleaning up (Deleting update files)");
    try {
      await fs.remove(this.path + "\\Mana2");
      await fs.remove(updateDestination);
      console.log("Cleanup done");
    } catch (err) {
      console.error(err);
      Status.showError(
        "Clean up Failed",
        "Please remove '" +
          updateDestination +
          "' and '" +
          "this.path" +
          "\\Mana2" +
          "' manualy",
        `Unzip Failed: ${err.message}`
      );
    }
    Status.setProgress(-1);
    //Status.showError("Unziping isn't implemented yet", "WIP", "not further implemented")
    Status.setActivity("Update successfull");

    return 0;
  }
  async updateAvailable(): Promise<{
    isNewVersion: boolean;
    newestVersion: string;
  }> {
    try {
      let versions = await getRequest(
        "https://tmw2.org/manalauncher/versions.json?" + Date.now()
      );
      let currect_version = (await this.isInstalled)
        ? await this.getVersion()
        : "-";
      return {
        isNewVersion:
          currect_version.indexOf(versions.windows64.version) === -1,
        newestVersion: versions.windows64.version,
      };
    } catch (e) {
      throw e;
    }
  }
}
