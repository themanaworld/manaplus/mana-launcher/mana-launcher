# A Launcher for ManaPlus

## Runnning and compiling

### Start instructions

```bash
npm i
npm start
```

### Package & Installer instructions

`npm run package` to package for all suported platforms at once

Info: you need `wine` to cross build from linux for windows (https://www.electron.build/multi-platform-build#linux)

## Contribution Guidelines

- We use prettier for code formatting, please run `npm run formatting:check` and `npm run formatting:fix` before commiting.

## Implemented Features

- A server selection that contains the socialLinks of the server and a short description

- Display every screenshot for each server in the launcher and enable drag them out of there in other directories
- View 20 latest Screenshots for one of the servers and a button to open the folder
- Save credentials for one given server and automatically login with those (password remembering broken with current manaplus version)

(On Windows)

- Start ManaPlus
- Download ManaPlus
- Open ManaPlus with Selected Server

(On Linux)

- Start ManaPlus
- Download ManaPlus
- Open ManaPlus with Selected Server

(On Mac)

## Planned Features

### TODO

- Do the client_data update with the Launcher and add ability to add simple "Texture packs" (the client_data and texturpack folder get merged an passed to the client)

- Login with Game Account (only TMW2 server)
  - enable rich presence with Information - where your are on the map

### Plan / important

- Log in with your game account

  - For saving the login information
  - And for other things like that the launcher has access to the login so that he can send a teleport request to the server (required for the rich present thing)

- adding custom gameservers

### Idea-State

- Keyboard shortcut to show a game helper windows that can show different information and maybe even do stuff InGame (like a join event button that teleports you to the event)
- viewer for Ingame screenshots that allows dragging images out in discord or on the desktop and **publish** them for use on the website after they got reviewed
- join party via discord

- Discord Rich Present
  - joining a Event with teleporting there (if logged in into the launcher)
  - Show on which server the player plays
  - Show in which world the Player is (if logged in into the launcher) {works that the launcher connects to the gameserver and asks him where the player is}

## Notes / command reference

this for icon?:``--icon=src/assets/images/icon.ico`
